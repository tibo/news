<?php
class SaveAction extends CAction
{
    public function run($type=null, $id=null)
    {
    	$controller=$this->getController();
        $result=array("result"=>false, "msg"=>Yii::t("common","You must be logged in to add a news entry !"));
    	if(Person::logguedAndValid()){
            $result=News::save($_POST);
        	if(!@$_REQUEST["json"]){
        		$params=array(
        			"news"=>array( (string)$result["id"]=>$result["object"]), 
        			"actionController"=>"save",
        			"canManageNews"=>true,
        			"canPostNews"=>true,
                    "endStream"=>false,
                    "pair" => false);
    			echo $controller->renderPartial("news.views.co.timelineTree", $params,true);
        	}else
                return Rest::json($result);
        }else
            return Rest::json($result);
    }
}